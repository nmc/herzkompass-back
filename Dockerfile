FROM archlinux/base
EXPOSE 8000

RUN pacman -Syyu --noconfirm
RUN pacman -S --noconfirm iputils python python-pip
RUN pacman -S --noconfirm gcc linux-headers openjpeg2 postgresql apache
RUN pacman -S --noconfirm tar

# Variable django environment
ARG djangoenv=openshift
ENV DJANGO_ENV=$djangoenv

# Create virtualenv
RUN mkdir -p /opt

# Create source code environment
RUN mkdir -p /opt/src
RUN mkdir -p /opt/media 
WORKDIR /opt/src
ADD . ./
RUN ls -al ./

# prepare log folder
RUN mkdir /opt/log
RUN chmod -R a+w /opt/log

# install django app and 
RUN sh -c "pip install -r requirements/openshift.txt"

# we dont need static files for an api, lets get rid of it
# collect static files, set dummy values for env variables defined in openshit_safe
# RUN sh -c "\
# 	    DB_USER= DB_NAME= DB_PASSWORD= MAILJET_API_KEY= MAILJET_API_SECRET= DJANGO_SECRET_KEY=palimpalim \
# 	    python /opt/src/herzkompass/manage.py collectstatic \
# 	    --no-input \
# 	    --settings=herzkompass.settings.openshift_safe \
# 	    "

WORKDIR /opt/src/herzkompass

USER 1001

CMD bash -c "\
      python manage.py runmodwsgi \
       --url-alias /static /opt/static \
       --url-alias /media /opt/media \
       --log-to-terminal \
       --settings=herzkompass.settings.openshift_safe \
       "
