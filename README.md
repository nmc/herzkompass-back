# Herzkompass Backend with Django-REST API


## Installation

Detailed, up to date instructions can be found in this projects [Dockerfile](./Dockerfile).

## Set up data

The bash script ./setUpFreshDB.sh includes the steps to create the necessary tables and fill the db with test data on a local setup. This works well on a sqlite database but was reported to fail on a postgres db.

Don't forget the projects python virtual environment before applying the script. If not applyied on a local setup, pass the django settings as parameter.

~~~ bash
setUpFreshDB.sh herzkompass.settings.openshift
~~~

## Run locally

After sourcing into the python virtual environment:

~~~ bash
python herzkompass/manage.py runserver
~~~

## Run docker test with global settings locally

~~~ bash
cd test
docker-compose -f docker-compose.yml run test-backend
