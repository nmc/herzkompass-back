import os
import logging
from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings


class DefaultAccountAdapterCustom(DefaultAccountAdapter):

    def get_email_confirmation_url(self, request, emailconfirmation):
        """Constructs our custom frontend email confirmation (activation) url.
        https://stackoverflow.com/questions/27984901/how-to-customize-activate-url-on-django-allauth
        """

        logger = logging.getLogger('django')
        logger.info("mail logger")
        logger.info(emailconfirmation.email_address)

        url = os.path.join(
            settings.FRONTEND_URL,
            settings.FRONTEND_ACCOUNT_ACTIVATION_ROUTE,
            emailconfirmation.key
        )

        logger.info(url)

        return url

    def respond_email_verification_sent(self, request, user):
        """ In the original adapter, a reverse to a 'verification email sent'
        view template is stated here. We dont need this in rest-api
        environement """
        pass
