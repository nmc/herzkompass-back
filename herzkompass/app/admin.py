from django.contrib import admin

# Register your models here.
from . import models


class HeartDiseaseCategoryInline(admin.TabularInline):
    model = models.HeartDiseaseCategory.heartdiseases.through


class HeartDiseaseDiagnosisInline(admin.TabularInline):
    model = models.HeartDisease_Diagnosis


class HeartDiseaseAdmin(admin.ModelAdmin):
    model = models.HeartDisease
    inlines = [
        HeartDiseaseCategoryInline,
    ]


class HeartOperationCategoryInline(admin.TabularInline):
    model = models.HeartOperationCategory.heartoperations.through


class HeartOperationInterventionInline(admin.TabularInline):
    model = models.HeartOperation_Intervention


class HeartOperationAdmin(admin.ModelAdmin):
    model = models.HeartOperation
    inlines = [
        HeartOperationCategoryInline,
    ]


class MedicalInfoAdmin(admin.ModelAdmin):
    model = models.MedicalInfo


class RelativeInline(admin.TabularInline):
    model = models.Relative


admin.site.register(models.HeartOperation, HeartOperationAdmin)
admin.site.register(models.HeartOperationCategory)
admin.site.register(models.HeartOperation_Intervention)
admin.site.register(models.HeartDisease, HeartDiseaseAdmin)
admin.site.register(models.HeartDiseaseCategory)
admin.site.register(models.HeartDisease_Diagnosis)

admin.site.register(models.MedicalInfo, MedicalInfoAdmin)
admin.site.register(models.HeartCenter)
