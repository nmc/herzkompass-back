from django.apps import AppConfig


class RestApiConfig(AppConfig):
    name = 'app'

    def ready(self):
        import app.signals
