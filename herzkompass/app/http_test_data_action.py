import json
import os
import re

from django.conf import settings
from django.core import mail
from django.core.management.base import BaseCommand

from rest_framework.test import APIClient

test_file_path = os.path.join(
    settings.BASE_DIR, '../app/data/test_patienten.json'
)


class HttpTestDataAction(BaseCommand):
    help = 'Fill db with test data'

    def __init__(self, *args, **kwargs):
        super(HttpTestDataAction, self).__init__(*args, **kwargs)

        setattr(
            settings,
            'EMAIL_BACKEND',
            'django.core.mail.backends.locmem.EmailBackend'
        )

        self.patient_list = json.load(
            open(test_file_path)
        )
        self.current_user_pk = 0
        self.endpoints = [
            {
                "name": "medical-info",
                "path": "/medical-info/"
            }, {
                "name": "heart-intervention",
                "path": "/heart-operation-intervention/"
            }, {
                "name": "heart-diagnosis",
                "path": "/heart-disease-diagnosis/"
            }, {
                "name": "relatives",
                "path": "/relative/"
            }
        ]

        self.client = APIClient()
        pass

    def register_user(self, patient):
            data = {}
            data["email"] = patient["email"]
            data["password1"] = patient["first_name"] + patient["last_name"]
            data["password2"] = data["password1"]
            data["first_name"] = patient["first_name"]
            data["last_name"] = patient["last_name"]
            return self.client.post(
                '/rest-auth/registration/', data, format='json'
            )

    def verify_user(self):
        # Get the activation email.
        first_email = mail.outbox[0]

        activation_link = re.findall(
            r'[A-Za-z0-9]*:[A-Za-z0-9]{6}:[A-Za-z0-9_-]{27}',
            first_email.body
        )[0]

        del mail.outbox[0]

        data = {
            'key': activation_link,
        }

        return self.client.post(
            '/rest-auth/registration/verify-email/', data, format='json'
        )

    def login_user(self, patient):
            data = {}
            data["email"] = patient["email"]
            data["password"] = patient["first_name"] + patient["last_name"]
            response = self.client.post(
                '/rest-auth/login/', data, format='json'
            )
            self.client.credentials(
                HTTP_AUTHORIZATION='Token ' + response.data['key']
            )
            return response

    def put_data(self, data, endpoint):
            payload = {}

            self.stdout.write(self.style.NOTICE(
                "\nPut data to endpoint %s" % endpoint
            ))
            for key, value in data.items():
                if not isinstance(value, dict):
                    payload[key] = value

            response = self.client.put(endpoint, payload, format='json')
            return response

    def patch_data(self, data, endpoint):
            payload = {}

            self.stdout.write(self.style.NOTICE(
                "\nPatch data to endpoint %s" % endpoint
            ))
            for key, value in data.items():
                if not isinstance(value, dict):
                    payload[key] = value

            self.stdout.write(self.style.NOTICE(
                "Sending: %s\n" % str(payload)
            ))
            response = self.client.patch(endpoint, payload, format='json')
            self.stdout.write(self.style.NOTICE(
                "Got response: %s\n" % str(response.data)
            ))
            return response

    def post_data(self, data, endpoint):
            self.stdout.write(self.style.NOTICE(
                "Posting to endpoint %s:\n%s" % (
                    endpoint, json.dumps(data, indent=4, sort_keys=True)
                )
            ))

            response = self.client.post(endpoint, data, format='json')
            self.stdout.write(self.style.NOTICE(
                "Got response: %s\n" % str(response)
            ))
            return response

    def delete_data(self, endpoint):
        return self.client.delete(endpoint, format='json')

    def get_data(self, endpoint):
        return self.client.get(endpoint, format='json')

    def sign_up_and_log_in(self, user_data):
        self.register_user(user_data)
        self.verify_user()
        return self.login_user(user_data)

    def post_one_user(self, user_data):
        self.stdout.write(self.style.NOTICE(
            "\n\nPosting data of %s:\n" % user_data['email']
        ))
        self.sign_up_and_log_in(user_data)
        userData = self.get_data('/rest-auth/user/').data
        try:
            self.patch_data(user_data["userdata"], "/users/%s/" %
                            userData['pk'])
        except KeyError:
            self.stdout.write(self.style.WARNING(
                "No userdata found for %s." % user_data['email']
            ))
            pass
        for endpoint in self.endpoints:
            payload = {}
            try:
                fileData = user_data[endpoint["name"]]
            except KeyError:
                self.stdout.write(self.style.WARNING(
                    "No %s data found for %s." %
                    (endpoint["name"], user_data['email'])
                ))
                pass
            else:
                if endpoint["name"] is not "relatives":
                    payload = fileData
                    payload['user'] = userData['pk']
                    self.post_data(payload, endpoint["path"])
                else:
                    for relative in fileData:
                        payload = relative
                        payload['user'] = userData['pk']
                        self.post_data(payload, endpoint['path'])

    def post_all_users(self):

        for idx, patient in enumerate(self.patient_list):
            self.post_one_user(patient)

        self.stdout.write(self.style.SUCCESS('posted all clients!'))
