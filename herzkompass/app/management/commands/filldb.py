from django.core.management.base import BaseCommand
from app import http_test_data_action


class Command(BaseCommand):

    def handle(self, *test_labels, **options):
        filldb_obj = http_test_data_action.HttpTestDataAction()
        filldb_obj.post_all_users()
        pass
