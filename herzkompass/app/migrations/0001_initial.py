# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-26 19:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HeartDisease_Diagnosis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.CharField(blank=True, max_length=255, verbose_name="Description of patient's heart disease diagnosis")),
            ],
        ),
        migrations.CreateModel(
            name='HeartDisease',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('illustration_url', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='HeartOperation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('illustration_url', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='HeartOperation_Intervention',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.CharField(blank=True, max_length=255, verbose_name="Description of patient's heart operation")),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cardialogist', models.CharField(blank=True, max_length=255, verbose_name='Full name of the cardialogist')),
                ('family_doctor', models.CharField(blank=True, max_length=255, verbose_name='Full name of the family doctor or medical center')),
                ('care_contact', models.CharField(blank=True, max_length=255, verbose_name='Name and phone number of medical carer')),
                ('diagnosis', models.CharField(default='', max_length=255)),
                ('secondary_diagnosis', models.CharField(blank=True, default='', max_length=255)),
                ('complication', models.CharField(blank=True, default='', max_length=255)),
                ('intervention', models.CharField(blank=True, default='', max_length=255)),
                ('allergy', models.CharField(blank=True, default='', max_length=255)),
                ('noncardial_diagnosis', models.CharField(blank=True, default='', max_length=255)),
                ('drug', models.CharField(blank=True, default='', max_length=255)),
                ('oral_anticoagulation', models.CharField(blank=True, default='', max_length=255)),
                ('endocarditis_prophylaxis', models.CharField(blank=True, default='', max_length=255)),
                ('icd', models.BooleanField(default=False)),
                ('pacemaker', models.BooleanField(default=False)),
                ('sport', models.CharField(blank=True, default='', max_length=255)),
                ('contraception', models.CharField(blank=True, default='', max_length=255)),
                ('air_filter', models.CharField(blank=True, default='', max_length=255)),
                ('living_will', models.BooleanField(default=False)),
                ('organ_donor_card', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Relative',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=16, verbose_name='Full name of relative')),
                ('relation', models.CharField(blank=True, max_length=16, verbose_name='Relation to the patient')),
                ('phone', models.CharField(blank=True, max_length=16, verbose_name='Phone number of relative')),
            ],
        ),
    ]
