# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-20 14:54
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_auto_20170920_1423'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='heartdisease_diagnosis',
            options={'permissions': (('view_heartdisease_diagnosis', 'View Heart Disease Diagnosis'),)},
        ),
        migrations.AlterModelOptions(
            name='heartoperation_intervention',
            options={'permissions': (('view_heartoperation_intervention', 'View Heart Operation Intervention '),)},
        ),
    ]
