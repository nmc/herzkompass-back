# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-11-08 12:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0031_auto_20171103_1925'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='relative',
            options={'permissions': (('view_relative', 'View Relative'),)},
        ),
    ]
