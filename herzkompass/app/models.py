from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class MedicalInfo(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='medical_info',
        null=True
    )
    cardiologist = models.CharField(
        _('Full name of the cardiologist'),
        max_length=255,
        blank=True,
    )
    family_doctor = models.CharField(
        _('Full name of the family doctor or medical center'),
        max_length=255,
        blank=True,
    )
    care_contact = models.CharField(
        _('Name and phone number of medical carer'),
        blank=True,
        max_length=255
    )
    complication = models.CharField(
        max_length=255,
        blank=True,
    )
    allergy = models.CharField(
        max_length=255,
        blank=True,
    )
    noncardiac_diagnosis = models.CharField(
        max_length=255,
        blank=True,
    )
    drug = models.TextField(
        blank=True,
    )
    oral_anticoagulation = models.CharField(
        max_length=255,
        blank=True,
    )
    endocarditis_prophylaxis = models.NullBooleanField()
    icd = models.NullBooleanField()
    icd_brand_model = models.CharField(
        max_length=255,
        blank=True,
    )
    pacemaker = models.NullBooleanField()
    pacemaker_brand_model = models.CharField(
        max_length=255,
        blank=True,
    )
    sport = models.CharField(
        max_length=255,
        blank=True
    )
    contraception = models.CharField(
        max_length=255,
        blank=True,
    )
    air_filter = models.NullBooleanField()
    living_will = models.NullBooleanField()
    organ_donor_card = models.NullBooleanField()

    def __str__(self):
        return self.user.email


class HeartCenter(models.Model):
    name = models.CharField(
        _("Full name of the hospital or heart center"),
        blank=True,
        max_length=255
    )

    short_name = models.CharField(
        _("Short name or abbreviation of the hospital or heart center, used "
          "to generate group names"),
        unique=True,
        max_length=255
    )

    address = models.CharField(
        _('Complete address of the heart center'),
        max_length=256, blank=True
    )
    phone = models.CharField(_('phone'), max_length=256, blank=True)

    user_list = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        "heart_center_list",
        verbose_name=_("Users that are associated with the heart center"),
        blank=True
    )

    def __str__(self):
        return self.short_name


class HeartDisease(models.Model):
    name = models.CharField(max_length=255)
    long_name = models.CharField(max_length=255)
    illustration_url = models.URLField()

    def __str__(self):
        return self.name


class HeartDiseaseCategory(models.Model):
    name = models.CharField(max_length=255)
    heartdiseases = models.ManyToManyField(
        HeartDisease,
        related_name='categories',
        blank=True
    )

    def __str__(self):
        return self.name


class HeartOperation(models.Model):
    name = models.CharField(max_length=255)
    long_name = models.CharField(max_length=255)
    illustration_url = models.URLField()

    def __str__(self):
        return self.name


class HeartOperationCategory(models.Model):
    name = models.CharField(max_length=255)
    heartoperations = models.ManyToManyField(
        HeartOperation,
        related_name='categories',
        blank=True
    )

    def __str__(self):
        return self.name


class HeartDisease_Diagnosis(models.Model):
    notes = models.CharField(
        _('Description of patient\'s heart disease diagnosis'),
        blank=True, max_length=255
    )
    heart_disease = models.ForeignKey(
        HeartDisease,
        on_delete=models.CASCADE,
        null=True
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="heart_diagnosis",
        null=True
    )

    def __str__(self):
        return self.notes


class HeartOperation_Intervention(models.Model):
    notes = models.CharField(
        _('Description of patient\'s heart operation'),
        blank=True, max_length=255
    )
    heart_operation = models.ForeignKey(
            HeartOperation,
            on_delete=models.CASCADE,
            null=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="heart_intervention",
        blank=True,
        null=True
    )

    def __str__(self):
        return self.notes


class Relative(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='relatives',
        blank=True,
        null=True,
    )
    name = models.CharField(
        _('Full name of relative'),
        blank=True, max_length=255
    )
    relation = models.CharField(
        _('Relation to the patient'),
        blank=True, max_length=255
    )
    phone = models.CharField(
        _('Phone number of relative'),
        blank=True, max_length=255
    )

    email = models.EmailField(
        _('email address'),
        blank=True
    )

    def __str__(self):
        return self.name
