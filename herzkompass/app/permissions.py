from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from guardian.shortcuts import remove_perm
from guardian.shortcuts import assign_perm
from rest_framework.permissions import DjangoObjectPermissions


class DjangoViewObjectPermissions(DjangoObjectPermissions):
    """
    force view permissions for get/options/head requsts
    """
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


def add_perms_of_model_to_user_or_group(appLabel, obj, userOrGroup):
    model_name = obj._meta.model_name
    assign_perm("%s.view_%s" % (appLabel, model_name), userOrGroup)
    assign_perm("%s.change_%s" % (appLabel, model_name), userOrGroup)
    assign_perm("%s.delete_%s" % (appLabel, model_name), userOrGroup)


def add_perms_of_object_to_user_or_group(obj, userOrGroup):
    model_name = obj._meta.model_name
    assign_perm("view_%s" % model_name, userOrGroup, obj)
    assign_perm("change_%s" % model_name, userOrGroup, obj)
    assign_perm("delete_%s" % model_name, userOrGroup, obj)
    

def remove_user_from_heart_center(heartCenterShortName, userObj):
    expert_group = Group.objects.get(name="%s_expert" %
                                     heartCenterShortName)
    admin_group = Group.objects.get(name="%s_admin" %
                                    heartCenterShortName)
    remove_perm("view_user", expert_group, userObj)
    remove_perm("view_user", admin_group, userObj)
    remove_perm("change_user", expert_group, userObj)
    remove_perm("change_user", admin_group, userObj)
    remove_perm("delete_user", expert_group, userObj)
    remove_perm("delete_user", admin_group, userObj)

    # remove rights of user's objects to heart center expert_group
    try:
        medical_info = userObj.medical_info
    except get_user_model().medical_info.RelatedObjectDoesNotExist:
        pass
    else:
        remove_perm("view_medicalinfo", expert_group, medical_info)
        remove_perm("change_medicalinfo", expert_group, medical_info)
        remove_perm("delete_medicalinfo", expert_group, medical_info)

    try:
        heart_intervention = userObj.heart_intervention
    except get_user_model().heart_intervention.RelatedObjectDoesNotExist:
        pass
    else:
        remove_perm("view_heartoperation_intervention", expert_group,
                    heart_intervention)
        remove_perm("change_heartoperation_intervention", expert_group,
                    heart_intervention)
        remove_perm("delete_heartoperation_intervention", expert_group,
                    heart_intervention)

    try:
        heart_diagnosis = userObj.heart_diagnosis
    except get_user_model().heart_diagnosis.RelatedObjectDoesNotExist:
        pass
    else:
        remove_perm("view_heartdisease_diagnosis", expert_group,
                    heart_diagnosis)
        remove_perm("change_heartdisease_diagnosis", expert_group,
                    heart_diagnosis)
        remove_perm("delete_heartdisease_diagnosis", expert_group,
                    heart_diagnosis)
