from allauth.account import app_settings as allauth_settings
from allauth.utils import (email_address_exists,
                           get_username_max_length)
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from app.models import (MedicalInfo, HeartOperation, HeartDisease,
                        HeartDiseaseCategory, HeartDisease_Diagnosis,
                        HeartOperation_Intervention, HeartOperationCategory,
                        HeartCenter, Relative)
from .permissions import (remove_user_from_heart_center,
                          add_perms_of_object_to_user_or_group,
                          add_perms_of_model_to_user_or_group)
import logging


def is_expert(request):
    '''checks whether the user making this request is in an expert group'''
    is_expert = False
    user_groups = request.user.groups.all()
    for group in user_groups:
        if group.name.endswith('expert'):
            is_expert = True
    return is_expert


def is_admin(user):
    '''checks whether the user making this request is in an admin group'''
    is_admin = False
    user_groups = user.groups.all()
    for group in user_groups:
        if group.name.endswith('admin'):
            is_admin = True
    return is_admin


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)

    def to_representation(self, value):
        # our groups are concatenations of the hospital name and the role with
        # an underscore in between.
        role = value.name.split("_")[-1]
        return role

    def to_internal_value(self, value):
        return str(value)


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(
        many=True,
        required=False
    )

    class Meta:
        model = get_user_model()
        fields = (
            'pk', 'id', 'email', 'first_name', 'last_name',
            'heart_center_list', 'medical_info', 'groups',
            'heart_intervention', 'heart_diagnosis', 'address', 'phone',
            'birthday', 'verified', 'relatives'
        )
        extra_kwargs = {
            'heart_center_list': {
                'required': False,
                'allow_null': True,
                'many': True
            },
            'medical_info': {
                'allow_null': True,
                'required': False,
            },
            'heart_diagnosis': {
                'required': False,
                'allow_null': True,
            },
            'heart_intervention': {
                'required': False,
                'allow_null': True,
            }
        }

    def update(self, instance, validated_data):
        user = self.context['request'].user

        try:
            groups = validated_data.pop('groups')
        except KeyError:
            pass
        else:
            if (is_admin(user)):  # only admins are allowed to change groups
                for heartCenter in user.heart_center_list.all():
                    if 'expert' in groups:
                        # the requester (admin) can up/downgrade users to
                        # the expert posistion of heart centers he is
                        # employed at
                        instance.groups.add(Group.objects.get(
                            name="%s_expert" % heartCenter.short_name))
                    else:
                        instance.groups.remove(Group.objects.get(
                            name="%s_expert" % heartCenter.short_name))

        try:
            heartCenterListNew = validated_data['heart_center_list']
        except KeyError:
            pass
        else:
            heartCenterListOld = instance.heart_center_list.all()
            diff = set(heartCenterListOld).difference(heartCenterListNew)
            if len(diff) > 0:
                # user removed a heart center
                for heartCenter in diff:
                    remove_user_from_heart_center(heartCenter.short_name,
                                                  instance)
                    instance.groups.remove(
                        Group.objects.get(name="%s_expert" %
                                          heartCenter.short_name),
                        Group.objects.get(name="%s_admin" %
                                          heartCenter.short_name)
                    )

        for attr, value in validated_data.items():
            # some values can be null, eg if the patient doesnt have had a
            # heart op the frontend sends the attrbute 'heart_operation: null'
            try:
                setattr(instance, attr, value)
            except TypeError:
                # this is necessary since django doesnt allow direct setattr of
                # manyToMany fields anymore
                attr2 = getattr(instance, attr)
                attr2.set(value)
                pass
            except AttributeError:
                pass

        instance.save()
        return instance


class SendFeedbackSerializer(serializers.Serializer):

    email = serializers.EmailField(required=True)
    message = serializers.CharField(required=True, allow_blank=True)
    feedbackType = serializers.CharField(required=True)

    def send_feedback(self):
        if self.is_valid():
            sender = self.validated_data['email']
            feedbackType = self.validated_data['feedbackType']
            message = self.validated_data['message']
            feedbackData = {
                'administrative': [
                    'zah@insel.ch',
                    '%s hat eine administrative Frage zum Heartapp' % sender
                ],
                'technical': [
                    'services-nmc@unibas.ch',
                    '%s hat eine technische Frage zum Heartapp' % sender
                ],
                'opinion': [
                    'zah@insel.ch',
                    '%s gibt Feedback zum Heartapp' % sender
                ],
                'deleteMe': [
                    'services-nmc@unibas.ch',
                    '%s möchte seine Daten aus dem Heartapp löschen' % sender
                ]
            }


            try:
                mailData = feedbackData[feedbackType]
            except KeyError:
                subject = (
                    "%s hat einen nicht existierenden Feedback-Typ angegeben" %
                    sender
                )
                receiver = feedbackData['technical'][0]
            else:
                subject = mailData[1]
                receiver = mailData[0]

            if (feedbackType == 'deleteMe'):
                # we want to know if the user is authenticated and authorized
                logger = logging.getLogger('django')
                requestingUser = self.context['request'].user
                try:
                    requestingUser.email == sender
                except AttributeError:
                    logger.warning('%s hat versucht einen Benutzer zu löschen' %
                           requestingUser)
                    return
                else:
                    if (requestingUser.email != sender):
                        logger.warning("""Der authentifizierte Benutzer %s hat eine
                               deleteMe Anfrage mit email %s als Absender
                               geschickt""" % (requestingUser.email, sender))
                        # a user should not be able to delete data of somebody
                        # else
                        return

            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [receiver, ])


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(
        max_length=get_username_max_length(),
        min_length=allauth_settings.USERNAME_MIN_LENGTH,
        required=allauth_settings.USERNAME_REQUIRED
    )
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    def validate_username(self, username):
        username = get_adapter().clean_username(username)
        return username

    def validate_email(self, email):
        email = get_adapter().clean_email(email)
        if allauth_settings.UNIQUE_EMAIL:
            if email and email_address_exists(email):
                raise serializers.ValidationError(
                    _("A user is already registered with this e-mail address.")
                )
        return email

    def validate_password1(self, password):
        return get_adapter().clean_password(password)

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError(
                _("The two password fields didn't match.")
            )
        return data

    def custom_signup(self, request, user):
        pass

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', '')
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user


class RelativeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Relative
        fields = '__all__'

    def validate_user(self, value):
        reqUser = self.context['request'].user
        if (value.pk != reqUser.pk and not
                reqUser.has_perm('users.change_user', value)):
            raise serializers.ValidationError(
                "Related and requesting user are different")

        return value

    def create(self, validated_data):
        relativeObj = Relative.objects.create(**validated_data)
        add_perms_of_object_to_user_or_group(relativeObj, relativeObj.user)
        add_perms_of_model_to_user_or_group('app', relativeObj,
                                            relativeObj.user)

        user = validated_data['user']

        for heartCenter in user.heart_center_list.all():
            group = Group.objects.get(name="%s_expert" %
                                      heartCenter.short_name)
            add_perms_of_object_to_user_or_group(relativeObj, group)
            add_perms_of_model_to_user_or_group('app', relativeObj, group)

        return relativeObj


class HeartDiseaseCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = HeartDiseaseCategory
        fields = '__all__'


class HeartDiseaseSerializer(serializers.ModelSerializer):
    categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = HeartDisease
        fields = ('id', 'illustration_url',
                  'name', 'name_de', 'name_en', 'name_fr',
                  'long_name', 'long_name_de', 'long_name_en', 'long_name_fr',
                  'categories')


class HeartDiseaseDiagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeartDisease_Diagnosis
        fields = '__all__'

    def update(self, instance, validated_data):

        for attr, value in validated_data.items():
            try:
                setattr(instance, attr, value)
            except AttributeError:
                pass

        # Check whether the user who us making changes is an expert, if not
        # set the meidcal profiles verified state to False
        if not is_expert(self.context['request']):
            instance.user.verified = False
            instance.user.save()

        instance.save()
        return instance


class HeartOperationCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = HeartOperationCategory
        fields = '__all__'


class HeartOperationSerializer(serializers.HyperlinkedModelSerializer):
    categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = HeartOperation
        fields = ('id', 'illustration_url', 'name', 'name_de', 'name_en',
                  'name_fr', 'long_name', 'long_name_de', 'long_name_en',
                  'long_name_fr', 'categories')

    def update(self, instance, validated_data):

        for attr, value in validated_data.items():
            setattr(instance, attr, value)

        # Check whether the user who us making changes is an expert, if not
        # set the meidcal profiles verified state to False
        if not is_expert(self.context['request']):
            instance.user.verified = False
            instance.user.save()

        instance.save()
        return instance


class HeartOperationInterventionSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeartOperation_Intervention
        fields = '__all__'

    def update(self, instance, validated_data):

        for attr, value in validated_data.items():
            try:
                setattr(instance, attr, value)
            except AttributeError:
                pass

        # Check whether the user whi us making changes is an expert, if not
        # set the medical profiles verified state to False
        if not is_expert(self.context['request']):
            instance.user.verified = False
            instance.user.save()

        instance.save()
        return instance


class MedicalInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalInfo
        fields = (
            'id', 'sport', 'cardiologist', 'family_doctor', 'care_contact',
            'complication', 'allergy', 'noncardiac_diagnosis', 'drug',
            'oral_anticoagulation', 'endocarditis_prophylaxis',
            'icd', 'icd_brand_model', 'pacemaker', 'pacemaker_brand_model',
            'contraception', 'air_filter', 'living_will',
            'organ_donor_card', 'user'
        )

    def update(self, instance, validated_data):

        for attr, value in validated_data.items():
            try:
                setattr(instance, attr, value)
            except AttributeError:
                pass

        # Check whether the user who us making changes is an expert, if not
        # set the meidcal profiles verified state to False
        if not is_expert(self.context['request']):
            instance.user.verified = False
            instance.user.save()

        instance.save()
        return instance


class HeartCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeartCenter
        exclude = ('user_list',)
