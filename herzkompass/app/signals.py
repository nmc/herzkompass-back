from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.dispatch import receiver
from django.db.models.signals import post_save

from guardian.shortcuts import assign_perm
from guardian.conf import settings as guardian_settings

from .models import (
    HeartCenter, HeartDisease_Diagnosis,
    HeartOperation_Intervention, MedicalInfo,
    Relative
)
from .permissions import (add_perms_of_object_to_user_or_group,
                          add_perms_of_model_to_user_or_group)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def user_post_save(sender, **kwargs):
    user, created = kwargs["instance"], kwargs["created"]

    if created and user.email != guardian_settings.ANONYMOUS_USER_NAME:
        assign_perm("users.add_user", user)
        add_perms_of_object_to_user_or_group(user, user)
        assign_perm("add_user", user, user)
        add_perms_of_model_to_user_or_group('users', user, user)
        assign_perm("app.add_relative", user)
        add_perms_of_model_to_user_or_group('app', Relative, user)

    elif not created and user.email != guardian_settings.ANONYMOUS_USER_NAME:
        # user is updated
        for heart_center in user.heart_center_list.all():
            expert_group = Group.objects.get(
                name="%s_expert" % heart_center.short_name
            )
            admin_group = Group.objects.get(
                name="%s_admin" % heart_center.short_name
            )
            heart_center.user_list.add(user)
            # assign rights of user object to heart center experts and admins
            add_perms_of_object_to_user_or_group(user, expert_group)
            add_perms_of_object_to_user_or_group(user, admin_group)

            # assign rights of user's objects to heart center expert_group
            try:
                medical_info = user.medical_info
            except get_user_model().medical_info.RelatedObjectDoesNotExist:
                pass
            else:
                add_perms_of_object_to_user_or_group(medical_info,
                                                     expert_group)
            try:
                heart_intervention = user.heart_intervention
            except (get_user_model()
                    .heart_intervention.RelatedObjectDoesNotExist):
                pass
            else:
                add_perms_of_object_to_user_or_group(heart_intervention,
                                                     expert_group)
            try:
                heart_diagnosis = user.heart_diagnosis
            except get_user_model().heart_diagnosis.RelatedObjectDoesNotExist:
                pass
            else:
                add_perms_of_object_to_user_or_group(heart_diagnosis,
                                                     expert_group)
            try:
                relativeList = user.relatives.all()
            except get_user_model().relatives.RelatedObjectDoesNotExist:
                pass
            else:
                for relative in relativeList:
                    relObj = Relative.objects.get(pk=relative.pk)

                    add_perms_of_object_to_user_or_group(relObj,
                                                         expert_group)


@receiver(post_save, sender=MedicalInfo)
def medical_info_post_save(sender, **kwargs):
    medical_info, created = kwargs["instance"], kwargs["created"]
    if created:
        user = medical_info.user
        add_perms_of_object_to_user_or_group(medical_info, user)
        add_perms_of_model_to_user_or_group('app', medical_info, user)


@receiver(post_save, sender=HeartDisease_Diagnosis)
def heart_diagnosis_post_save(sender, **kwargs):
    heart_diagnosis, created = kwargs["instance"], kwargs["created"]
    if created:
        user = heart_diagnosis.user
        add_perms_of_object_to_user_or_group(heart_diagnosis, user)
        add_perms_of_model_to_user_or_group('app', heart_diagnosis, user)


@receiver(post_save, sender=HeartOperation_Intervention)
def heart_intervention_post_save(sender, **kwargs):
    heart_intervention, created = kwargs["instance"], kwargs["created"]
    if created:
        user = heart_intervention.user
        add_perms_of_object_to_user_or_group(heart_intervention, user)
        add_perms_of_model_to_user_or_group('app', heart_intervention, user)


@receiver(post_save, sender=HeartCenter)
def heart_center_post_save(sender, **kwargs):
    heart_center, created = kwargs["instance"], kwargs["created"]
    if created:
        group = Group.objects.create(
            name="%s_admin" % heart_center.short_name
        )
        group.save()
        group = Group.objects.create(
            name="%s_expert" % heart_center.short_name
        )
        group.save()
        add_perms_of_model_to_user_or_group('app', Relative, group)
