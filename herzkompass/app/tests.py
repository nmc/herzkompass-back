from allauth.account.models import EmailAddress
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core import mail
from app import http_test_data_action
from app import models


def setUpExpertsAndAdmins():
    admin = get_user_model().objects.create_user(
        email="admin@insel.ch",
        first_name="Admin",
        last_name="Insel",
        password="AdminInsel"
    )
    expert = get_user_model().objects.create_user(
        email="expert@insel.ch",
        first_name="Expert",
        last_name="Insel",
        password="ExpertInsel"
    )
    expert.groups.add(Group.objects.get(name="insel_expert"))
    admin.groups.add(Group.objects.get(name="insel_admin"))
    expert.save()
    admin.save()
    EmailAddress.objects.create(user=admin, email="admin@insel.ch",
                                primary=True, verified=True)
    EmailAddress.objects.create(user=expert, email="expert@insel.ch",
                                primary=True, verified=True)

    admin = get_user_model().objects.create_user(
        email="admin@ukbb.ch",
        first_name="Admin",
        last_name="Ukbb",
        password="AdminUkbb"
    )
    expert = get_user_model().objects.create_user(
        email="expert@ukbb.ch",
        first_name="Expert",
        last_name="Ukbb",
        password="ExpertUkbb"
    )
    expert.groups.add(Group.objects.get(name="ukbb_expert"))
    admin.groups.add(Group.objects.get(name="ukbb_admin"))
    expert.save()
    admin.save()
    EmailAddress.objects.create(user=admin, email="admin@ukbb.ch",
                                primary=True, verified=True)
    EmailAddress.objects.create(user=expert, email="expert@ukbb.ch",
                                primary=True, verified=True)


def setUpHeartCenters():
    models.HeartCenter.objects.create(
        name="Universitäts-Kinderspital beider Basel",
        short_name="ukbb",
        address=(u"""Universitäts-Kinderspital beider Basel\n
                     Kardiologie
                     \nSpitalstrasse 33\nCH-4056 Basel"""),
        phone="+41 61 704 12 12"
    )
    models.HeartCenter.objects.create(
        name="Inselspital Bern",
        short_name="insel",
        address=(u"""Zentrum für angeborene Herzfehler\n
                Universitätsklinik für Kardiologie\n
                Freiburgstrasse\n3010 Bern"""),
        phone="+41 31 632 78"
    )


class RegisterUserTest(TestCase):
    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.albi = self.action.patient_list[0]
        self.babsi = self.action.patient_list[1]

    def test_register(self):
        # Try to register a single user
        response = self.action.register_user(self.albi)
        self.assertEqual(response.status_code, 201)

        emailAddressObject = EmailAddress.objects.get(
            email=self.albi["email"]
        )
        self.assertEquals(emailAddressObject.verified, False)
        response = self.action.verify_user()
        self.assertEqual(response.status_code, 200)
        emailAddressObject = EmailAddress.objects.get(
            email=self.albi["email"]
        )
        self.assertEquals(emailAddressObject.verified, True)


class PostPatientData(TestCase):
    fixtures = [
        "heartdisease_short", "heartdiseasecategory", "heartoperation_short",
        "heartoperationcategory", "heartcenter"
    ]

    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()

    def test_post_all_users(self):
        self.action.post_all_users()
        albi = get_user_model().objects.get(email="albert@amann.ch")
        albi_medical_info = models.MedicalInfo.objects.get(user=albi)
        self.assertEquals(albi_medical_info.sport, "keine Einschränkungen")
        self.assertEquals(len(albi.relatives.all()), 2)

    def test_post_heart_diagnosis_first(self):
        self.action.sign_up_and_log_in({
            "email": "albert@amann.ch",
            "first_name": "Albert",
            "last_name": "Amann"
        })
        albiId = self.action.get_data('/rest-auth/user/').data['pk']

        heartDiseasedId = self.action.post_data(
            {"user": albiId},
            '/heart-disease-diagnosis/').data['id']

        self.action.patch_data({
            "heart_disease": 20,
            "notes": "noch keine Dataen"
        }, '/heart-disease-diagnosis/%s/' % heartDiseasedId)

        albi = get_user_model().objects.get(email="albert@amann.ch")
        self.assertEqual(albi.heart_diagnosis.heart_disease.name,
                         'PAPVC to RA')

    def test_post_relative(self):
        self.action.sign_up_and_log_in({
            "email": "albert@amann.ch",
            "first_name": "Albert",
            "last_name": "Amann"
        })
        albiId = self.action.get_data('/rest-auth/user/').data['pk']

        # we need a user object since it holds relatives

        self.action.post_data({
            "user": albiId,
            "name": "",
            "relation": "Vater",
            "phone": "078 457 63 24"
        }, '/relative/')

        albi = get_user_model().objects.get(email="albert@amann.ch")
        self.assertEqual(albi.relatives.all().first().relation, 'Vater')

    def test_post_heart_center(self):
        self.action.sign_up_and_log_in({
            "email": "albert@amann.ch",
            "first_name": "Albert",
            "last_name": "Amann"
        })

        hc_list = self.action.get_data('/heart-center/').data
        hc0 = hc_list[0]  # should be insel spital
        albi = self.action.get_data('/rest-auth/user/').data
        # no hc chosen yet
        self.assertEqual(len(albi["heart_center_list"]), 0)
        self.action.patch_data({"heart_center_list": [hc0["id"]]},
                               '/users/%s/' % albi['pk'])
        albi = self.action.get_data('/rest-auth/user/').data
        self.assertEqual((albi["heart_center_list"][0]), hc0['id'])


class ExpertUserPower(TestCase):
    fixtures = [
        "heartdisease_short", "heartdiseasecategory", "heartoperation_short",
        "heartoperationcategory"
    ]

    @classmethod
    def setUpTestData(cls):
        setUpHeartCenters()
        setUpExpertsAndAdmins()

    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.insel_expert_credentials = {
            "email": "expert@insel.ch",
            "first_name": "Expert",
            "last_name": "Insel"
        }
        self.action.post_all_users()

    def patient_picks_hospital(self):
        # insel expert logs in and cant see anyone but himself
        self.action.login_user(self.insel_expert_credentials)
        self.assertEquals(len(self.action.get_data('/users/').data), 1)

        # Albert joins insel hospital
        albi = self.action.patient_list[0]
        self.action.login_user(albi)
        insel = models.HeartCenter.objects.get(short_name="insel")
        data = {"heart_center_list": [insel.pk]}
        self.action.patch_data(data, '/rest-auth/user/')

        # now insel expert sees that Albi has joined his hospital
        self.action.login_user(self.insel_expert_credentials)
        self.assertEquals(len(self.action.get_data('/users/').data), 2)

    def test_user_joins_expert_group(self):
        # Albert joins insel hospital
        albi = self.action.patient_list[0]
        self.action.login_user(albi)
        insel = models.HeartCenter.objects.get(short_name="insel")
        data = {
            "heart_center_list": [insel.pk],
        }
        self.action.patch_data(data, '/rest-auth/user/')

        # new expert registers
        expert_data = {
            "email": "die@fcbasel.com",
            "first_name": "geoffroy",
            "last_name": "serey die"
        }
        self.action.sign_up_and_log_in(expert_data)

        # now there are geoffroy, admin, experts, anonymouse and 8 test users
        self.assertEquals(len(get_user_model().objects.all()), 14)
        # expert can see himself singly
        self.assertEquals(len(self.action.get_data('/users/').data), 1)

        expert = get_user_model().objects.get(email="die@fcbasel.com")
        inselgroup = Group.objects.get(name="insel_expert")
        expert.groups.add(inselgroup)

        # expert can see patients associated with insel (albert) as well
        self.assertEquals(len(self.action.get_data('/users/').data), 2)
        # he can see albi's associated data as well
        self.assertEquals(len(self.action.get_data('/medical-info/').data), 1)
        self.assertEquals(len(self.action.get_data(
            '/heart-disease-diagnosis/').data), 1)
        self.assertEquals(len(self.action.get_data(
            '/heart-operation-intervention/').data), 1)

        # albi leaves heart center, expert wont be able to see him anymore
        self.action.login_user(albi)
        self.action.patch_data({"heart_center_list": []}, '/rest-auth/user/')

        # expert cannot see anything of albi anymore
        self.action.login_user(expert_data)
        self.assertEquals(len(self.action.get_data('/users/').data), 1)
        self.assertEquals(len(self.action.get_data('/medical-info/').data), 0)
        self.assertEquals(len(self.action.get_data('/heart-disease-diagnosis/').data), 0)
        self.assertEquals(len(self.action.get_data('/heart-operation-intervention/').data), 0)

    def test_expert_verifies_user(self):
        self.patient_picks_hospital()
        albiObj = get_user_model().objects.get(email="albert@amann.ch")
        albiId = albiObj.pk
        albiMedId = albiObj.medical_info.id
        albi = self.action.get_data('/users/%s/' % albiId).data
        self.assertEquals(albi['verified'], False)
        self.action.patch_data({'verified': True}, '/users/%s/' % albiId)
        albi = self.action.get_data('/users/%s/' % albiId).data
        self.assertEquals(albi['verified'], True)
        # after a patient change his med profile 'verified' must be False again
        self.action.login_user(self.action.patient_list[0])
        self.action.patch_data({'drug': 'Alberteli Kicker'},
                               '/medical-info/%s/' % albiMedId)
        albiMed = self.action.get_data('/medical-info/%s/' % albiMedId).data
        self.assertEquals(albiMed['drug'], 'Alberteli Kicker')
        albi = self.action.get_data('/users/%s/' % albiId).data
        self.assertEquals(albi['verified'], False)


class AdminUserPower(TestCase):
    fixtures = [
        "heartdisease_short", "heartdiseasecategory", "heartoperation_short",
        "heartoperationcategory"
    ]

    @classmethod
    def setUpTestData(cls):
        setUpHeartCenters()
        setUpExpertsAndAdmins()

    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.insel_admin_credentials = {
            "email": "admin@insel.ch",
            "first_name": "Admin",
            "last_name": "Insel"
        }

        self.ukbb_admin_credentials = {
            "email": "admin@ukbb.ch",
            "first_name": "Admin",
            "last_name": "Ukbb"
        }

        self.babsiData = {
            "email": "barbara@baldinger.ch",
            "first_name": "Barbara",
            "last_name": "Baldinger",
        }
        self.carloData = {
            "email": "carlo@christen.ch",
            "first_name": "Carlo",
            "last_name": "Christen",
        }
        self.action.post_all_users()

    def test_admin_finds_himself_only(self):
        self.action.login_user(self.insel_admin_credentials)
        # no one at InselSpital, admin can get himself only
        self.assertEquals(len(self.action.get_data('/users/').data), 1)

        # carlo joins Insel
        self.action.login_user(self.carloData)
        self.assertEquals(len(self.action.get_data('/rest-auth/user/')
                              .data['heart_center_list']), 0)
        self.action.get_data('/rest-auth/user/').data
        insel = models.HeartCenter.objects.get(short_name="insel")
        ukbb = models.HeartCenter.objects.get(short_name="ukbb")
        self.action.patch_data(
            {"heart_center_list": [insel.pk]}, '/rest-auth/user/')
        self.assertEquals(len(self.action.get_data('/rest-auth/user/')
                              .data['heart_center_list']), 1)

        # admin can see himself and carlo
        self.action.login_user(self.insel_admin_credentials)
        self.assertEquals(len(self.action.get_data('/users/').data), 2)

        # Babsi logs in and joins Insel
        self.action.login_user(self.babsiData)
        self.assertEquals(len(self.action.get_data('/rest-auth/user/')
                              .data['heart_center_list']), 0)
        self.action.get_data('/rest-auth/user/').data
        self.action.patch_data(
            {"heart_center_list": [insel.pk]}, '/rest-auth/user/')
        self.assertEquals(len(self.action.get_data('/rest-auth/user/')
                              .data['heart_center_list']), 1)

        # admin can see himself, babsi and carlo
        self.action.login_user(self.insel_admin_credentials)
        self.assertEquals(len(self.action.get_data('/users/').data), 3)
        # admin adds himself to Insel
        self.action.patch_data({"heart_center_list": [insel.pk]},
                               '/rest-auth/user/')

        # Carlo is promoted to expert position
        carloPk = get_user_model().objects.get(email='carlo@christen.ch').pk
        self.action.patch_data({"groups": ['expert']}, '/users/%s/' % carloPk)
        self.assertEquals(self.action.get_data('/users/%s/' % carloPk)
                          .data['groups'][0], 'expert')

        # Carlo joins ukbb as well
        self.action.login_user(self.carloData)
        self.action.patch_data({"heart_center_list": [insel.pk, ukbb.pk]},
                               '/rest-auth/user/')
        self.assertEquals(len(self.action.get_data('/rest-auth/user/')
                              .data['heart_center_list']), 2)

        # ukbb admin logs in
        self.action.login_user(self.ukbb_admin_credentials)
        self.assertEquals(len(self.action.get_data('/users/').data), 2)
        # admin adds himself to ukbb
        self.action.patch_data({"heart_center_list": [ukbb.pk]},
                               '/rest-auth/user/')

        # Carlo is promoted to expert at ukbb as well
        self.action.patch_data({"groups": ['expert']}, '/users/%s/' % carloPk)
        self.assertEquals(len(self.action.get_data('/users/%s/' % carloPk)
                          .data['groups']), 2)

        # ukbb amdin removes Carlo from expert group, he remain in insel
        # admin group
        self.action.patch_data({"groups": []}, '/users/%s/' % carloPk)
        self.assertEquals(len(self.action.get_data('/users/%s/' % carloPk)
                          .data['groups']), 1)

    def test_admin_leaves_heartcenter(self):
        self.action.login_user(self.insel_admin_credentials)
        ukbb = models.HeartCenter.objects.get(short_name="ukbb")
        # admin adds himself to ukbb
        self.action.patch_data({"heart_center_list": [ukbb.pk]},
                               '/rest-auth/user/')
        adminJson = self.action.get_data('/users/').data
        self.assertEquals(adminJson[0]['groups'][0], "admin")
        # ukbb admin leaves ukbb
        self.action.patch_data({"heart_center_list": []},
                               '/rest-auth/user/')
        adminJson = self.action.get_data('/users/').data
        self.assertEquals(len(adminJson[0]['groups']), 1)


class Relatives(TestCase):
    fixtures = [
        "heartdisease_short", "heartdiseasecategory", "heartoperation_short",
        "heartoperationcategory"
    ]

    @classmethod
    def setUpTestData(cls):
        setUpHeartCenters()
        setUpExpertsAndAdmins()

    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.inselExpertCredentials = {
            "email": "expert@insel.ch",
            "first_name": "Expert",
            "last_name": "Insel"
        }
        self.albiCredentials = {
            "email": "albert@amann.ch",
            "first_name": "Albert",
            "last_name": "Amann"
        }
        self.carloCredentials = {
            "email": "carlo@christen.ch",
            "first_name": "Carlo",
            "last_name": "Christen",
        }
        self.action.post_all_users()
        # Add Carlo to Insel
        insel = models.HeartCenter.objects.get(short_name="insel")
        self.action.login_user(self.carloCredentials)
        self.action.patch_data({"heart_center_list": [insel.pk]},
                               '/rest-auth/user/')

    def test_add_relative(self):
        # expert sees himself and Carlo who joined Insel before
        self.action.login_user(self.inselExpertCredentials)
        self.assertEquals(len(self.action.get_data('/users/').data), 2)

        # Carlo has 0 relatives
        carlo = get_user_model().objects.get(email="carlo@christen.ch")
        self.assertEquals(len(carlo.relatives.all()), 0)

    def test_albi_cannot_add_relatives_for_carlo(self):
        carlo = get_user_model().objects.get(email="carlo@christen.ch")
        self.assertEquals(len(carlo.relatives.all()), 0)
        # Albi tries to add a relative for Carlo, this should fail
        self.action.login_user(self.albiCredentials)
        self.action.post_data({
            "user": carlo.pk,
            "name": "Carlo Senior",
            "relation": "Vater",
            "phone": "078 457 63 24"
        }, '/relative/')
        self.assertEquals(len(carlo.relatives.all()), 0)

    def test_carlo_can_add_his_father(self):
        carlo = get_user_model().objects.get(email="carlo@christen.ch")
        # Now Carlos tries to add a relative, which should succeed
        self.assertEquals(len(carlo.relatives.all()), 0)
        self.action.login_user(self.carloCredentials)
        self.action.post_data({
            "user": carlo.pk,
            "name": "Carlo Senior",
            "relation": "Vater",
            "phone": "078 457 63 24"
        }, '/relative/')
        self.assertEquals(len(carlo.relatives.all()), 1)

    def test_expert_can_see_father_of_carlo(self):
        carlo = get_user_model().objects.get(email="carlo@christen.ch")

        self.action.login_user(self.inselExpertCredentials)
        relativesTotal = self.action.get_data('/relative/').data
        relativesOfCarlo = [rel for rel in relativesTotal if rel['user'] ==
                            carlo.id]
        self.assertEquals(len(relativesOfCarlo), 0)
        self.action.login_user(self.carloCredentials)
        self.action.post_data({
            "user": carlo.pk,
            "name": "Carlo Senior",
            "relation": "Vater",
            "phone": "078 457 63 24"
        }, '/relative/')
        self.action.login_user(self.inselExpertCredentials)
        relativesTotal = self.action.get_data('/relative/').data
        relativesOfCarlo = [rel for rel in relativesTotal if rel['user'] ==
                            carlo.pk]
        self.assertEquals(len(relativesOfCarlo), 1)
        self.assertEquals(relativesOfCarlo[0]['relation'], 'Vater')

    def test_expert_can_post_relatives_of_carlo(self):
        carlo = get_user_model().objects.get(email="carlo@christen.ch")
        self.action.login_user(self.inselExpertCredentials)
        self.action.post_data({
            "user": carlo.pk,
            "name": "Carlo Seniorita",
            "relation": "Mudda",
            "phone": "078 457 63 34"
        }, '/relative/')
        relativesTotal = self.action.get_data('/relative/').data
        relativesOfCarlo = [rel for rel in relativesTotal if rel['user'] ==
                            carlo.pk]

        self.assertEquals(len(relativesOfCarlo), 1)
        self.assertEquals(relativesOfCarlo[0]['relation'], 'Mudda')

        self.action.login_user(self.carloCredentials)
        relativesOfCarlo = self.action.get_data('/relative/').data
        self.assertEquals(len(relativesOfCarlo), 1)
        self.assertEquals(relativesOfCarlo[0]['relation'], 'Mudda')


class Debugging(TestCase):
    fixtures = [
        "heartdisease_short", "heartdiseasecategory", "heartoperation_short",
        "heartoperationcategory"
    ]

    @classmethod
    def setUpTestData(cls):
        setUpHeartCenters()
        setUpExpertsAndAdmins()

    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.insel_expert_credentials = {
            "email": "expert@insel.ch",
            "first_name": "Expert",
            "last_name": "Insel"
        }

        self.action.post_all_users()

    def test_send_heartcenter_and_group_at_once(self):
        # 34-auswahlen-eines-herzzentrums-funktioniert-in-openshift-umgebung-manchmal-nicht-siehe-stacktrace
        # problem discovered by ms thomet. if patient is expert/admin an and
        # sends a heart center list, an error occurs
        self.action.login_user(self.insel_expert_credentials)
        # firstly check if the expert is really expert and has no hc attached
        expert = self.action.get_data('/rest-auth/user/').data
        self.assertEqual(len(expert['heart_center_list']), 0)
        self.assertEqual(expert['groups'][0], 'expert')

        insel = models.HeartCenter.objects.get(short_name="insel")
        # this is where the error happened, send hc and groups at once:
        self.action.patch_data({
            "heart_center_list": [insel.pk],
            "groups": ["expert"]
        }, '/rest-auth/user/')
        expert = self.action.get_data('/rest-auth/user/').data
        # see if both, group and insel are present:
        self.assertEqual(expert['heart_center_list'][0], insel.pk)
        self.assertEqual(expert['groups'][0], 'expert')


class PersonalInfo(TestCase):
    fixtures = [
        "heartdisease_short", "heartdiseasecategory", "heartoperation_short",
        "heartoperationcategory", "heartcenter"
    ]

    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.albi = self.action.patient_list[0]
        self.babs = self.action.patient_list[1]

    def test_albi_data_post(self):
        self.action.post_one_user(self.albi)
        albi = (get_user_model()
                .objects
                .get(email="albert@amann.ch"))
        self.assertTrue(isinstance(albi, get_user_model()))

    def test_albi_data_put(self):
        self.action.post_one_user(self.albi)
        albi = (get_user_model()
                .objects
                .get(email="albert@amann.ch"))
        self.action.patch_data(self.albi, '/users/{0}/'.format(albi.pk))
        updated_albi = (get_user_model()
                        .objects
                        .get(email="albert@amann.ch"))
        self.assertEquals(updated_albi.first_name, 'Albert')

    def test_albi_delete(self):
        allUsers = get_user_model().objects.all()
        # import pdb
        # pdb.set_trace()
        self.assertEquals(len(allUsers), 1)
        self.action.post_one_user(self.albi)
        allUsers = get_user_model().objects.all()
        self.assertEquals(len(allUsers), 2)
        albi = (get_user_model()
                .objects
                .get(email="albert@amann.ch"))

        # babs tries to delete albi
        self.action.post_one_user(self.babs)
        allUsers = get_user_model().objects.all()
        self.assertEquals(len(allUsers), 3)
        self.action.delete_data('/users/{0}/'.format(albi.pk))
        allUsers = get_user_model().objects.all()
        self.assertEquals(len(allUsers), 3)

        # albi deletes himself
        self.action.login_user(self.albi)
        self.action.delete_data('/users/{0}/'.format(albi.pk))
        allUsers = get_user_model().objects.all()
        self.assertEquals(len(allUsers), 2)


class SendFeedbackTest(TestCase):
    def setUp(self):
        self.action = http_test_data_action.HttpTestDataAction()
        self.albi = self.action.patient_list[0]

    def test_feedback(self):
        # Try to register a single user
        response = self.action.post_data({
            "email": "hexe@lebkuchenhaus.ch",
            "feedbackType": "opinion",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        self.assertEqual(response.status_code, 200)
        outbox = mail.outbox.pop()
        self.assertEqual(
            outbox.subject,
            'hexe@lebkuchenhaus.ch gibt Feedback zum Heartapp'
        )
        self.assertEqual(outbox.to[0], 'zah@insel.ch')
        self.assertEqual(outbox.body, 'Ganz lässiges App!')

        response = self.action.post_data({
            "email": "hexe@lebkuchenhaus.ch",
            "feedbackType": "administrative",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        self.assertEqual(response.status_code, 200)
        outbox = mail.outbox.pop()
        self.assertEqual(
            outbox.subject,
            'hexe@lebkuchenhaus.ch hat eine administrative Frage zum Heartapp'
        )
        self.assertEqual(outbox.to[0], 'zah@insel.ch')
        self.assertEqual(outbox.body, 'Ganz lässiges App!')

        response = self.action.post_data({
            "email": "hexe@lebkuchenhaus.ch",
            "feedbackType": "asd",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        outbox = mail.outbox.pop()
        self.assertEqual(
            outbox.subject,
            'hexe@lebkuchenhaus.ch hat einen nicht existierenden Feedback-Typ angegeben'
        )
        self.assertEqual(outbox.to[0], 'services-nmc@unibas.ch')
        self.assertEqual(outbox.body, 'Ganz lässiges App!')

        response = self.action.post_data({
            "email": "hexe@lebkuchenhaus.ch",
            "feedbackType": "technical",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        outbox = mail.outbox.pop()
        self.assertEqual(
            outbox.subject,
            'hexe@lebkuchenhaus.ch hat eine technische Frage zum Heartapp'
        )
        self.assertEqual(outbox.to[0], 'services-nmc@unibas.ch')
        self.assertEqual(outbox.body, 'Ganz lässiges App!')

        response = self.action.post_data({
            "email": "berta@merta.ch",
            "feedbackType": "deleteMe",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        outbox = mail.outbox.pop()
        self.assertEqual(
            outbox.subject,
            "[Django] WARNING: AnonymousUser hat versucht einen Benutzer zu löschen"
        )
        self.assertEqual(outbox.to[0], 'services-nmc@unibas.ch')

        self.action.post_one_user(self.albi)
        response = self.action.post_data({
            "email": "berta@merta.ch",
            "feedbackType": "deleteMe",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        outbox = mail.outbox.pop()
        self.assertEqual(
            "[Django] WARNING: Der authentifizierte Benutzer albert@amann.ch"
            in outbox.subject, True 
        )
        self.assertEqual(outbox.to[0], 'services-nmc@unibas.ch')

        response = self.action.post_data({
            "email": "albert@amann.ch",
            "feedbackType": "deleteMe",
            "message": "Ganz lässiges App!"
        }, '/feedback/')
        outbox = mail.outbox.pop()
        self.assertEqual(
            outbox.subject, 
            "albert@amann.ch möchte seine Daten aus dem Heartapp löschen"
        )
        self.assertEqual(outbox.to[0], 'services-nmc@unibas.ch')
