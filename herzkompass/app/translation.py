from modeltranslation.translator import translator, TranslationOptions
from .models import (HeartDisease,
                     HeartOperation,
                     HeartDiseaseCategory,
                     HeartOperationCategory)


class HeartDiseaseTranslationOptions(TranslationOptions):
    fields = ('name', 'long_name')
    required_languages = ('en',)


class HeartOperationTranslationOptions(TranslationOptions):
    fields = ('name', 'long_name')
    required_languages = ('en',)


class HeartDiseaseCategoryTranslationOptions(TranslationOptions):
    fields = ('name',)
    required_languages = ('en',)


class HeartOperationCategoryTranslationOptions(TranslationOptions):
    fields = ('name',)
    required_languages = ('en',)


translator.register(HeartDisease, HeartDiseaseTranslationOptions)
translator.register(HeartOperation, HeartOperationTranslationOptions)
translator.register(HeartDiseaseCategory,
                    HeartDiseaseCategoryTranslationOptions)
translator.register(HeartOperationCategory,
                    HeartOperationCategoryTranslationOptions)
