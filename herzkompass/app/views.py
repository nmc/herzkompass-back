from django.contrib.auth import get_user_model
from django.views.generic import View
from django.http import HttpResponse
from django.core.mail import send_mail
from django.conf import settings

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework_guardian import filters

from app.models import (
    MedicalInfo, HeartDisease, HeartOperation,
    HeartDisease_Diagnosis, HeartOperation_Intervention, Relative,
    HeartDiseaseCategory, HeartOperationCategory,
)
from app.serializers import (
    HeartDiseaseSerializer, UserSerializer,
    MedicalInfoSerializer, HeartOperationSerializer,
    HeartDiseaseDiagnosisSerializer, HeartOperationInterventionSerializer,
    HeartCenterSerializer, HeartCenter, RelativeSerializer,
    HeartDiseaseCategorySerializer, HeartOperationCategorySerializer,
    SendFeedbackSerializer
)


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.ObjectPermissionsFilter,)


class RelativeViewSet(viewsets.ModelViewSet):
    queryset = Relative.objects.all()
    serializer_class = RelativeSerializer
    filter_backends = (filters.ObjectPermissionsFilter,)


class MedicalInfoViewSet(viewsets.ModelViewSet):
    queryset = MedicalInfo.objects.all()
    serializer_class = MedicalInfoSerializer
    filter_backends = (filters.ObjectPermissionsFilter,)


class HeartCenterViewSet(viewsets.ModelViewSet):
    queryset = HeartCenter.objects.all()
    serializer_class = HeartCenterSerializer


class HeartDiseaseViewSet(viewsets.ModelViewSet):
    queryset = HeartDisease.objects.all()
    serializer_class = HeartDiseaseSerializer


class HeartDiseaseCategoryViewSet(viewsets.ModelViewSet):
    queryset = HeartDiseaseCategory.objects.all()
    serializer_class = HeartDiseaseCategorySerializer


class HeartDiseaseDiagnosisViewSet(viewsets.ModelViewSet):
    queryset = HeartDisease_Diagnosis.objects.all()
    serializer_class = HeartDiseaseDiagnosisSerializer
    filter_backends = (filters.ObjectPermissionsFilter,)


class HeartOperationViewSet(viewsets.ModelViewSet):
    queryset = HeartOperation.objects.all()
    serializer_class = HeartOperationSerializer


class HeartOperationCategoryViewSet(viewsets.ModelViewSet):
    queryset = HeartOperationCategory.objects.all()
    serializer_class = HeartOperationCategorySerializer


class HeartOperationInterventionViewSet(viewsets.ModelViewSet):
    queryset = HeartOperation_Intervention.objects.all()
    serializer_class = HeartOperationInterventionSerializer
    filter_backends = (filters.ObjectPermissionsFilter,)


class SendFeedbackView(APIView):
    # Anybody may send feedback
    permission_classes = ()

    def post(self, request):
        serializer = SendFeedbackSerializer(data=request.data, context = {
            'request': request })
        serializer.send_feedback()
        return HttpResponse('Thanks for your feedback!')


class ReportErrorView(View):

    def post(self, request):
        # content = request.POST.dict().popitem()
        send_mail('HeartApp error report',
                  str(request.body, 'utf-8'),
                  settings.DEFAULT_FROM_EMAIL,
                  [settings.DEFAULT_ERROR_REPORT_RECIPIENT, ])
        return HttpResponse('Thanks for your feedback!')
