from .base_safe import *  # noqa: F403

DEBUG = True
ALLOWED_HOSTS = ['*']


CORS_ORIGIN_WHITELIST = [
        'http://localhost:8080'
        ]

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'OPTIONS': {
            'timeout': 500,
            }
    },
}

FRONTEND_URL = 'http://localhost:8080'
FRONTEND_HOST = 'localhost:8080'

ADMIN_URL = r'^admin/'

# email backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS.extend([
    'debug_toolbar',
    'django.contrib.messages',
    'django.contrib.admin'
])

MIDDLEWARE.extend([  # noqa: F405
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware'
])


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dont_tell_anyone'
LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'WARNING',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': False,
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}
