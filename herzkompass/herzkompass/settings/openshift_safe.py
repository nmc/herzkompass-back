import os
from .base_safe import *  # noqa: F403

# ALLOWED_HOSTS = ['*']
# CORS_ORIGIN_ALLOW_ALL = True

CORS_ORIGIN_WHITELIST = (
    'https://www.heartapp.org',
    'http://herzkompass-back',
    'https://herzkompass-front-unibas-app1.appuioapp.ch',
)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'USER': os.environ['DB_USER'],
        'NAME': os.environ['DB_NAME'],
        'HOST': 'postgresql',
        'PORT': '5432',
        'PASSWORD': os.environ['DB_PASSWORD'],
    }
}


FRONTEND_URL = 'https://www.heartapp.org'
FRONTEND_HOST = 'www.heartapp.org'

MEDIA_ROOT = os.path.join("/opt/media")
STATIC_ROOT = os.path.join("/opt/static")

MAILJET_API_KEY = os.environ['MAILJET_API_KEY']
MAILJET_API_SECRET = os.environ['MAILJET_API_SECRET']
# gehört zu whitenoise
# STATIC_HOST = 'https://api.heartapp.org'
# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
# STATIC_URL = STATIC_HOST + '/static/'

INSTALLED_APPS.extend(['mod_wsgi.server'])

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/opt/log/error.log',
            'maxBytes': 1024,
            'backupCount': 4,
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'WARNING',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': False,
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file', 'mail_admins'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}
