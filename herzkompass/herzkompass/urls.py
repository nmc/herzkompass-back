from django.conf import settings
from django.contrib import admin

from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt

from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
# from rest_framework_jwt.views import obtain_jwt_token

from app.views import (
    HeartDiseaseViewSet, HeartOperationViewSet,
    UserViewSet, MedicalInfoViewSet, HeartDiseaseDiagnosisViewSet,
    HeartOperationInterventionViewSet, HeartCenterViewSet,
    RelativeViewSet, HeartDiseaseCategoryViewSet,
    HeartOperationCategoryViewSet, ReportErrorView,
    SendFeedbackView
)

schema_view = get_schema_view(title='Heart disease API')

router = DefaultRouter()

router.register(r'heart-disease', HeartDiseaseViewSet)
router.register(r'heart-disease-category', HeartDiseaseCategoryViewSet)
router.register(r'heart-disease-diagnosis', HeartDiseaseDiagnosisViewSet)
router.register(r'heart-operation', HeartOperationViewSet)
router.register(r'heart-operation-category', HeartOperationCategoryViewSet)
router.register(
    r'heart-operation-intervention',
    HeartOperationInterventionViewSet
)
router.register(r'users', UserViewSet)
router.register(r'medical-info', MedicalInfoViewSet)
router.register(r'relative', RelativeViewSet)
router.register(r'heart-center', HeartCenterViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^schema/$', schema_view),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^reportErrors/', csrf_exempt(ReportErrorView.as_view())),
    url(r'^feedback/', csrf_exempt(SendFeedbackView.as_view())),
]


if settings.DEBUG:
    urlpatterns += url(settings.ADMIN_URL, admin.site.urls),
