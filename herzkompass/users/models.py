from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(username=email, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(
            email, password, **extra_fields
        )


class User(AbstractUser):
    """User model."""

    email = models.EmailField(
        _('email address'),
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )

    first_name = models.CharField(_('first name'), max_length=35)
    last_name = models.CharField(_('last name'), max_length=150)
    address = models.CharField(_('address'), max_length=256, blank=True)
    phone = models.CharField(_('phone'), max_length=256, blank=True)
    birthday = models.DateField(_('Date of birth'), null=True)

    verified = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        ordering = ["last_name", "email"]

    def __str__(self):
        return self.email


def get_anonymous_user_instance(User):
    """
    used by django-guardia that comes with a anonymous user that is
    different from the normal django anonymous user
    """
    return User(
        email=settings.ANONYMOUS_DEFAULT_USERNAME_VALUE,
        pk=-1,  # user_id -1 so anonymouse dosnt stand in real users way
    )
