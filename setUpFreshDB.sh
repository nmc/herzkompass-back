SETTINGS=herzkompass.settings.local
if [[ $# -gt 1 ]];
then
	printf "too many arguments\n"
elif [[ $# -eq 1 ]]
then
	SETTINGS=$1
fi

# First apply migrations
python herzkompass/manage.py migrate --settings=$SETTINGS
# load consistent app data (changeable by django-admin only, not by users)
python herzkompass/manage.py loaddata --settings=$SETTINGS \
	heartcenter\
	admin_and_experts\
	heartdisease_short\
	heartoperation_short\
	heartdiseasecategory\
	heartoperationcategory
# filldb will fill user data
python herzkompass/manage.py filldb --settings=$SETTINGS
