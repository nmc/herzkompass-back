import os
import psycopg2

connection_parameters = {
        'host': os.environ['DB_HOST'],
        'dbname': os.environ['DB_NAME'],
        'user': os.environ['DB_USER'],
        'password': os.environ['DB_PASSWORD']
    }

print(connection_parameters) 
conn = psycopg2.connect(**connection_parameters)
print(conn.info.dbname, conn.info.password, conn.info.user, conn.info.host) 

# ... some time elapses, e.g. connection within a connection pool
try:
    conn.isolation_level
except psycopg2.OperationalError as oe:
    conn = psycopg2.connect(**connection_parameters)

c = conn.cursor()
c.execute("SELECT 1")
