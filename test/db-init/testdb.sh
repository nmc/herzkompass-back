#!/bin/bash
set -e

echo "db USER: ${DB_USER}"
echo "db NAME: ${DB_NAME}"
echo "db PSWO: ${DB_PASSWORD}"

echo "postgres user: ${POSTGRES_USER}"
echo "postgres name: ${POSTGRES_DB}"
echo "postgres psw: ${POSTGRES_PASSWORD}"

env

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" --password "$POSTGRES_PASSWORD" <<-EOSQL
 SELECT 1;
EOSQL
