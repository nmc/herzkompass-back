import unittest
from handleEndPointPaths import create_list_of_routes_and_responses


class TestEndpoints(unittest.TestCase):
    def setUp(self):
        routes = create_list_of_routes_and_responses()
        self.validRouteList = routes[0]
        self.invalidRouteList = routes[1]

    def test_find_invalid_route(self):
        self.assertEqual(len(self.invalidRouteList), 0)
        if(len(self.invalidRouteList) > 0):
            print('Error, following routes are invalid:')
            for (rou, res) in self.invalidRouteList:
                print('{}\n{}'.format(rou, res.status_code))


if __name__ == '__main__':
    unittest.main()
