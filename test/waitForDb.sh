#!/bin/sh
# waitForDb.sh

set -e

cmd="$@"

THIS_FILE_PATH=$(dirname $(readlink -f $0))

counter=0
until python ${THIS_FILE_PATH}/checkPgConnection.py; do
  >&2 echo "DB is unavailable - sleeping"
  if [[ "${counter}" -gt 10 ]]; then
      ping postgresql -c 1
      echo "looped $counter times, exiting."
      exit 1
  fi
  counter=$((counter+1))
  sleep 1
done


>&2 echo "DB is up - executing command"

exec $cmd
